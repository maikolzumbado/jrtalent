'use strict';
const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
 
gulp.task('imagemin', () =>
    gulp.src('app/assets/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./build/images'))
);