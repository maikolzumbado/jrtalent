'use strict';
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;

gulp.task('serve', ['less','sass','pug'],function() {
    browserSync.init({
        server: {
            baseDir: ['./build','./app']
        }
    });
    gulp.watch('./app/assets/sass/**/*.scss', ['sass']); // ruta, task name
    gulp.watch('./app/assets/less/**/*.less', ['less']);
    gulp.watch(['./app/assets/js/**/*.js','./gulp/tasks/**/*.js'], ['lint','babel']);
    gulp.watch('./app/**/*.pug', ['pug']);
    gulp.watch(['./build/**/*.html','./build/css/**/*.css','./app/assets/js/**/*.js']).on('change', reload);
    
});