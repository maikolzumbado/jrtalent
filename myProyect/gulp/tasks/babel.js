const gulp = require('gulp');
const babel = require('gulp-babel');
 
gulp.task('babel', () => {
	console.log("Babel in action");
    return gulp.src('./app/assets/js/**/*.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('./build/js'));
});