'use strict';
var gulp = require('gulp');
var pug = require('gulp-pug');

//  pug
gulp.task('pug', function buildHTML() {
  return gulp.src('./app/pages/*.pug')
  .pipe(pug())
  .pipe(gulp.dest('./build'));
});

