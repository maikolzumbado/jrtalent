'use strict';

// Module alert 

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var myModule = function () {
	function myModule() {
		_classCallCheck(this, myModule);

		console.log('The script is done done');
	}

	_createClass(myModule, [{
		key: 'logSomething',
		value: function logSomething(txt) {
			console.log(txt);
		}
	}, {
		key: 'alertSomething',
		value: function alertSomething(txt) {
			alert(txt);
		}
	}]);

	return myModule;
}();

var mymoduleInstance = new myModule();
mymoduleInstance.alertSomething('HOLA JOSUE');

var isPalindrome = function isPalindrome(txt) {
	return prepTxt(txt) === [].concat(_toConsumableArray(prepTxt(txt))).reverse().join('');
};

var prepTxt = function prepTxt(txt) {
	return txt.replace(/[^A-Za-z0-9]/g, '').toLowerCase();
};

console.log(isPalindrome("ANA MASA LA MASA"));
/*
var myModule = (function(){
	//clousure

	const init = () => {
		console.log('The script is done done');
	};

	const consoleMe = (txt) =>{
		console.log(txt);
	};

	const alertMe = (txt) =>{
		alert(txt);
	};

	init();

	return {
	consoleMe,
	alertMe
	};

})();
*/