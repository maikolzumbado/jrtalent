'use strict';
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;
var sass        = require('gulp-sass');
var less = require('gulp-less');
var path = require('path');
var pug = require('gulp-pug');

// Module to require whole directories
var requireDir = require('require-dir');

// Pulling in all tasks from the tasks folder
requireDir('./gulp/tasks', { recurse: true });



gulp.task('sass', function  () {
  console.log('Compiling Saas');
  return gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('../build/css'))
    .pipe(browserSync.stream());
  // body...
});

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "../build"
        }
    });
    gulp.watch('./sass/*.scss', ['sass']); // ruta, task name
    gulp.watch('./less/*.less', ['less']);
    gulp.watch('./pages/*.pug', ['pug']);
    gulp.watch('./build/*.html').on('change', reload);
    
});

gulp.task('less', function () {
  return gulp.src('./less/**/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('../build/css'))
    .pipe(browserSync.stream());
});
//  pug
gulp.task('pug', function buildHTML() {
  return gulp.src('pages/*.pug')
  .pipe(pug())
  .pipe(gulp.dest('../build'));
});
