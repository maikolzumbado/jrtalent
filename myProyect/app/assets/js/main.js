'use strict';

// Module alert 

class myModule {
	constructor(){
		console.log('The script is done done');
	}
	logSomething(txt){
		console.log(txt);
	}
	alertSomething(txt){
		alert(txt);

	}
}
var mymoduleInstance = new myModule();
mymoduleInstance.alertSomething('HOLA JOSUE');

var isPalindrome = (txt) => 
    prepTxt(txt) === [...prepTxt(txt)].reverse().join('');

var prepTxt = (txt) => txt.replace(/[^A-Za-z0-9]/g, '').toLowerCase();

console.log(isPalindrome("ARRIBA LA BIIRA"));
/*
var myModule = (function(){
	//clousure

	const init = () => {
		console.log('The script is done done');
	};

	const consoleMe = (txt) =>{
		console.log(txt);
	};

	const alertMe = (txt) =>{
		alert(txt);
	};

	init();

	return {
	consoleMe,
	alertMe
	};

})();
*/