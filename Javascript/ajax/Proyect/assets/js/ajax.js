var loadItems = (function(){
  var loadDoc = function(){ 
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     console.log('Ajax ready');
    }
  };
  xhttp.open("GET", "https://randomuser.me/api/ ", true);
  xhttp.send();
  };
  return {
  	loadDoc: loadDoc
  };
})();

loadItems.loadDoc();