	
var loadUsers = (function(){
	var listElements = Array.prototype.slice.call(document.querySelectorAll("#list-elements li"));
	var loadDoc = function () {
	  var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	    
		document.getElementById('testText').innerText = xhttp.responseText;
		parseResponse(xhttp.responseText);
		console.log('Doing something');
	    }
	  };
	  xhttp.open("GET", "https://randomuser.me/api/ ", true);
	  xhttp.send();
	};
	function getElementByClass( className){
		var temp =  listElements.filter(function(x){
			return x.attributes['class'].value == className;
		});
		if(temp.length == 1){
			return temp[0];
		} else{
			// To do error handle 
		}
	}
	function parseResponse(element){

		var data = JSON.parse(element),
			imageUser = document.getElementById("img-user");

		
		imageUser.src = data.results[0].picture.large;
		getElementByClass('name').setAttribute("data-value",data.results[0].name.first);
		getElementByClass('phone').setAttribute("data-value",data.results[0].cell);

		getElementByClass('email').setAttribute("data-value",data.results[0].email);
		getElementByClass('birthday').setAttribute("data-value",data.results[0].dob);
		
		getElementByClass('address').setAttribute("data-value",data.results[0].location.city);
		getElementByClass('password').setAttribute("data-value",data.results[0].login.password);
		setText();
	};

	function setText(){
		var descriptionUser = document.getElementById("contact-description");
		var contentUser = document.getElementById("content-contact");
		descriptionUser.innerText = listElements[0].getAttribute("data-description");
		contentUser.innerText = listElements[0].getAttribute("data-value");
		listElements.forEach(function(el){
			el.addEventListener("mouseover", function(e) { 
				contentUser.innerText  = this.getAttribute("data-value");
				descriptionUser.innerText = this.getAttribute("data-description");
			});
		});
	}
	return { 
		loadDoc : loadDoc
	};	
})();
loadUsers.loadDoc();

document.getElementById('buttonNext').addEventListener("click",function(){
	loadUsers.loadDoc();
});
