

function addTaskToList(task){
	//var listElements = document.querySelector('#list-elements');
	//var listTask = document.querySelector('#list-elements > li:last-child');
	var inputTask = document.getElementById('task');
	if(inputTask.value.length > 0){
		var listTask = document.querySelector('.main-task__list');
		var newTask = document.createElement('div');
		var newTaskElements = document.createElement('span');
		var taskDone = document.createElement('input');
		var deleteTask = document.createElement('button');
		newTask.innerText = task;
		taskDone.setAttribute('type','checkbox');
		newTask.setAttribute('class','divCreate');
		newTaskElements.setAttribute('class','some');
		taskDone.setAttribute('class','checkBoxElement');
		deleteTask.setAttribute('class','btn btn-danger')
		deleteTask.innerText = 'X';
		newTaskElements.appendChild(taskDone);
		newTaskElements.appendChild(deleteTask);
		newTask.appendChild(newTaskElements); 
		listTask.appendChild(newTask);
		taskDone.addEventListener('click',function(e){
			console.log('Done task');

			newTask.className += ' task-complete';
			taskDone.setAttribute("disabled", "true");

		});
		deleteTask.addEventListener('click',function(){
			newTask.remove();
		})
	}
};


var addTask = document.getElementById('addTask');
addTask.addEventListener("click",function(e){
	var task = document.getElementById('task');
	var inputValue = document.getElementById('task').value;
	addTaskToList(inputValue);
	task.value = '';
});

document.getElementById('task__list').addEventListener("click", function(e) { 
	if(e.target && e.target.nodeName == "DIV") { 
		// List item found! Output the ID! 
		console.log("List item ", e.target.id.replace("post-", ""), " was clicked!"); 
		//e.target.remove();
		/*
		if(e.target.checkbox.check){
			// Task done underline task
			console.log('Underline');
		}
		if(e.target.button.clicked){
			// Delete task 
			console.log('Deleting');
			e.target.remove();
		}
		*/
	} 
});
