
/*function loadImage(url){ 
	var promise = new Promise( 
		function resolver(resolve, reject) { 
			var img = new Image(); 
			img.src = url; 
			
			img.onload = function() { 
				resolve(img); 
			}; 

			img.onerror = function() { 
				reject(e); 
			}; 
		} 
	); 
	return promise; 
*/
//1
 "use strict";
 var  promise  =   new   Promise(function ( fulfill , reject )  { 
  	setTimeout(function(){ fulfill("fulfill"); }, 3000);
 }).then(function(value) { console.log(value); })

//2
 "use strict";
 var  promise  =   new   Promise(function ( fulfill , reject )  { 
  	setTimeout(function(){ reject("Reject!"); }, 3000);
 }).catch(function (e){
 	reject(e); })

  function  reject  ( error )  {
  	console.log("Rejected!");
}

 //3
 var promise = new Promise( 
	function(resolve, reject) { 
		resolve(onResolve('I FIRED!')); 
		reject(new Error('I DID NO FIRED')); 
}).catch(function (e) { 
	reject(e);
}); 

 function  reject  ( error )  {
  	console.log(error.message);
}

function onResolve(value) { 
	console.log('Resolve with value' + value); 
};

//4
var promise = new Promise(
	function(resolve,reject){
		resolve(onResolve('Promise Value'));
	}).then(function(value) {  console.log('Resolve with value' + value);  });   

console.log('Main Program');

//5
var promise = new Promise(
	function(resolve, reject) { 
	// A mock async action using setTimeout 
		resolve(10); 
})
.then(function(num) { console.log('Which could be the secret Num ? If the result of adding 5 is 15'); return num + 5; }) 
.then(function(num) { console.log('Now the number is: ' + num + ' And I will multiplied with 5 and the result is 75 '); return num * 5; }) 
.then(function(num) { console.log('You have 10 seconds to discoverthe secretet number'); setTimeout(function(){ console.log('The secret number is : 10'); }, 10000);});

//6

var promise = new Promise(
	function(resolve, reject) { 
		resolve('MANHATTAN'); 
})
.then(function(result) { attachTitle(result) } ); 

function attachTitle(result) {  console.log('DR. ' + result); }

//7
var myJson = '{"employees":[' +
'{"firstName":"Josue","lastName":"Chinchilla" },' +
'{"firstName":"Maikol","lastName":"Zumbado" },' +
'{"firstName":"Issac","lastName":"Valverde" }]}';
var myData = '{"name":"Josue Chinchilla","Dept":"JR Talent","Age":"23"}';
function jsonParse(element){
var  promise  =   new   Promise(function (resolve, reject)  { 
		resolve(element);
 })
.then(function(element) {  parsePromised(element); })
.catch(function (e){
 	console.log(e.message); });


	function  parsePromised (element)  {
  		obj = JSON.parse(element);
		var result = obj.name + ' ' + obj.Dept + ' ' + obj.Age;
		console.log(result);
		return result;
	}
}
jsonParse(myData);
// 8

//WithOut Promise
function reqListener () {
  console.log(this.responseText);
}

var req = new XMLHttpRequest();
req.addEventListener("load", reqListener);
req.open("GET", "http://eloquentjavascript.net/author");
req.setRequestHeader('Accept', 'application/json');
req.send();

function loadDoc() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
        }
    };
    xhttp.open("GET", "http://eloquentjavascript.net/author", true);
    xhttp.setRequestHeader('Accept','application/json');
    xhttp.send();
}
loadDoc();

//With Promise
var promise = new Promise(function (resolve, reject){
	resolve(reqListener());
})
.then(function() { onResolve('application/json'); })
.catch(function(e){
	console.log(e.message);
}) 

function onResolve(typeDocument) {
var req = new XMLHttpRequest();
req.addEventListener("load", reqListener); //
req.open("GET", "http://eloquentjavascript.net/author");
req.setRequestHeader('Accept',typeDocument);
req.send();
}

function reqListener () {
  console.log(this.responseText);
}

//9
 var p1 = new Promise((resolve, reject) => { 
	setTimeout(resolve, 1000, "one"); 
}); 
var p2 = new Promise((resolve, reject) => { 
	setTimeout(resolve, 2000, "two"); 
}); 
var p3 = new Promise((resolve, reject) => { 
	setTimeout(resolve, 3000, "three"); 
}); 
var p4 = new Promise((resolve, reject) => { 
	setTimeout(resolve, 4000, "four"); 
}); 
var p5 = new Promise((resolve, reject) => { 
	//reject("reject"); 
	setTimeout(resolve, 5000, "five"); 

}); 

function all(promises)  {
 	var promise =  new   Promise (function (resolve,reject){
 	resolve(onResolve(promises));
  }).catch(function(e){
  	console.log(e);
  }); 
 }
 function onResolve(promises){
 	Promise.all(promises).then(value => { 
	console.log(value); 
	}, reason => { 
	console.log(reason) 
 });

 function onCatch(promises){
 	if(promises.length == 0){
 		console.log('This should be []');
 	}
 }

}
all([p1,p2,p3,p4,p5]);



Promise.all([p1, p2, p3, p4, p5]).then(value => { 
	console.log(value); 
}, reason => { 
	console.log(reason) 
}); 
//From console: //"reject"
function all(promises){
	return new Promise(function (success,fail){
		success(Promise.all(promises));
		}).catch(function(e){
			console.log(e.message);
		});
};
all([]).then(function(array) {
 console.log("This should be []:", array);
});

function soon(val) {
 return new Promise(function(success) {
 setTimeout(function() { success(val); },
 Math.random() * 500);
 });
}

all([soon(1), soon(2), soon(3)]).then(function(array) {
 console.log("This should be [1, 2, 3]:", array);
});

function fail() {
 return new Promise(function(success, fail) {
 fail(new Error("boom"));
 });
}

all([soon(1), fail(), soon(3)]).then(function(array) {
 console.log("We should not get here");
}, function(error) {
 if (error.message != "boom")
 console.log("Unexpected failure:", error);
});


