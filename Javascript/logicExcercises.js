var a = function (listNumber){
	if(listNumber.length > 0){
		var smaller = listNumber[0];
		for(var i = 0; i < listNumber.length; i++){
			if(listNumber[i] < smaller){
				smaller = listNumber[i];
			}
		}
	}
	return smaller;

}

console.log(a([8,3,1,2,8,0,9,10,11,12,13]));
//2
function reverseWord(word){
	var output = word.split('').reverse().join('');
	return output;
}
reverseWord('Hello');

function reverseSentece(element){
	var sentence = element.split(' ');
	var output = '';
	for(var i = 0; i < sentence.length; i++){
		//console.log(reverseWord(sentence[i]));
		output += reverseWord(sentence[i]);
		//console.log(output);
	}
	//return output;
	console.log(output);
}

reverseSentece('Hi Josue, I m Trying to output each word in reverse mode');
/*
function apply(number,element){
	
	var output = element.split(' ').map(function(){
		plusAsci(number,element);
	})
	console.log(output);
	return output;
} 
*/
//3
function getAsci(element){
	var result = element.charCodeAt();
	return result;
}

function plusAsci(number,element){
	var maxP = 122; // z
	var minP = 97; // a
	var asci = getAsci(element);
	var letterResult = asci + number;

	if(letterResult > maxP){
		var temp = letterResult - maxP  ;
		letterResult = minP + (temp - 1);
	}
	var res = String.fromCharCode(letterResult);
	return res;
}

function apply(number,element){

	var output = element.toLowerCase().split('');
	var result = '';
	for(var i = 0; i < output.length; i++){
		result += plusAsci(number,output[i]);
	}
	return result;
} 
apply(5,'vwxADGz');