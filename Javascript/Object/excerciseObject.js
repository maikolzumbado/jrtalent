// 1
var listProperty = function(value){
		return Object.keys(value);
}
var a = 'Hello World';
console.log(listProperty(a));

// 2
var deletePropperty = function(object){
		console.log(delete object.header);
}
var page = {
	html: {
		header: '<head></head>',
		body: '<body></body>',
		footer: '<footet></footer>'
	},
	assets: {
		images: {
			src: '../assets/images/car-1.png',
			alt: 'my fisrt car'
		},
		css: {
			src: '../assets/styles/main.css',
			file: 'main.css'
		},
		js: {
			src: '../assets/js/main.js',
			file: 'main.js'
		}
	},
	renderPage: function() { return this.html.header + ': ' + this.html.body + ': ' + this.html.header }
	
};

deletePropperty(page);

// 3
var student = {
       name : "David Rayy",
       sclass : "VI",
       rollno : 12
};

var lenghtObject = function(obj){
	var size = 0;
	for(key in obj)
		if (obj.hasOwnProperty(key)) size++;
	return size;
}
console.log(lenghtObject(student));

// 4

var library = [
{
    author: 'Bill Gates',
    title: 'The Road Ahead',
    readingStatus: true
},
{
	author: 'Steve Jobs',
	title: 'Walter Isaacson',
	readingStatus: true
},
{
	author: 'Suzanne Collins',
	title:  'Mockingjay: The Final Book of The Hunger Games',
	readingStatus: false
}];

var readObj = function(obj){
	for(i =0; i < obj.length; i++){
		console.log(obj[i].author);
		console.log(obj[i].title);
		console.log(obj[i].readingStatus);
	}
		
	//return value;
}
console.log(readObj(library));

//5 
var dog = 'dog';
var permutation = function(word){
	var array = [];
	var res;
	for(i=0; i < word.length+1; i++){
		for(var j = i+1; j < word.length+1; j++ ){
			res = word.substring(i, j);
			array.push(res);		
		}
	}
	return array;
}
permutation('dog'); 
// 6
/*
var pickuptime = {
	date : new Date(),
	hour : date.getHours(),
	minutes : date.getMinutes(),
	seconds : date.getSeconds(),
	realTimePrint : function() { console.log("Hour: " + hour + " Minutes: " + minutes + " Seconds: " + seconds); },
	executeTime : setInterval(function(){ console.log(realTimePrint()); }, 1000)
};

var localTime = Object.create(pickuptime);
localTime.executeTime();
*/

var updateTime = function(){
	//console.log(this.getHours());
	console.log("Hour: " + this.getHours() + " Minutes: " + this.getMinutes() + " Seconds: " + this.getSeconds());
}
var clock = {
	date : new Date(),
	realTime : function(){
		setInterval(updateTime.bind(this),1000);}
}

var excecuteClock = clock.realTime;
var boundFunction = excecuteClock.bind(clock);
boundFunction();


var pickuptime = function(){
    setInterval(function(){ 
    	var date = new Date();
		var hour = date.getHours();
		var minutes = date.getMinutes();
		var seconds = date.getSeconds();
		var realTime = "Hour: " + hour + " Minutes: " + minutes + " Seconds: " + seconds;
    	console.log(realTime); }, 1000);
}
pickuptime();

// 7
var library = [
 {
	 title: 'The Road Ahead',
	 author: 'Bill Gates',
	 libraryID: 1254
 },
 {
	 title: 'Walter Isaacson',
	 author: 'Steve Jobs',
	 libraryID: 4264
 },
 {
	 title: 'Mockingjay: The Final Book of The Hunger Games',
	 author: 'Suzanne Collins',
	 libraryID: 3245
 }];


var sortArrayObject = function(a,b) {
  if (a.libraryID < b.libraryID)
    return 1;
  if (a.libraryID > b.libraryID)
    return -1;
  return 0;
}

//console.log(library.sort(sortArrayObject));
var readObj = function(obj){
	library.sort(sortArrayObject);
	for(i =0; i < obj.length; i++){
		console.log(obj[i].title);
		console.log(obj[i].author);
		console.log(obj[i].libraryID);
	}
		
	//return value;
}
console.log(readObj(library));

// 8
var getFunctions = function(obj)
{
    var result = [];
    var properties = Object.getOwnPropertyNames(obj);
    for(var i = 0; i < properties.length; i++) {
    	console.log(obj[properties[i]]);
        if(typeof obj[properties[i]] == "function") {
            result.push(properties[i]);
        }
    }
    return result;
}

var page = {
	html: {
		header: '<head></head>',
		body: '<body></body>',
		footer: '<footet></footer>'
	},
	assets: {
		images: {
			src: '../assets/images/car-1.png',
			alt: 'my fisrt car'
		},
		css: {
			src: '../assets/styles/main.css',
			file: 'main.css'
		},
		js: {
			src: '../assets/js/main.js',
			file: 'main.js'
		}
	},
	renderPage: function() { return this.html.header + ': ' + this.html.body + ': ' + this.html.header }
	
};
//var arraya = ["hola","como estas"];
console.log(getFunctions(page));
console.log(getFunctions(Array));

//9
var page = {
	html: {
		header: '<head></head>',
		body: '<body></body>',
		footer: '<footet></footer>'
	},
	assets: {
		images: {
			src: '../assets/images/car-1.png',
			alt: 'my fisrt car'
		},
		css: {
			src: '../assets/styles/main.css',
			file: 'main.css'
		},
		js: {
			src: '../assets/js/main.js',
			file: 'main.js'
		}
	},
	renderPage: function() { return this.html.header + ': ' + this.html.body + ': ' + this.html.header }
	
};

var getValue = function(obj)
{
    var result = [];
    var properties = Object.getOwnPropertyNames(obj);
    for(var i = 0; i < properties.length; i++) {
    	//console.log(obj[properties[i]]);
    	if(typeof obj[properties[i]] != "function") {
            result.push(obj[properties[i]]);
        }

    }
    return result;
}
console.log(getValue(page));
// 10
var getValueKey = function(obj)
{
    var result = [];
    var properties = Object.getOwnPropertyNames(obj);
    for(var i = 0; i < properties.length; i++) {
    	if(typeof obj[properties[i]] != "function") {
    		var temp = [properties[i],obj[properties[i]]];
            result.push(temp);
        }

    }
    return result;
}
var book = {
    author: 'Bill Gates',
    title: 'The Road Ahead',
    readingStatus: true
};
console.log(getValueKey(book));
