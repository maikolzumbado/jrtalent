// 1
var Person = {
	name : 'Default name'
};
var Teacher = Object.create(Person, { 
	teach: { 
		value: function(subject) { return console.log(this.name + ' is teaching ' + subject); } 
	}
}); 
var michael = Object.create(Teacher);
michael.name = "Michael"; 
michael.teach("Data Base II"); 
var josue = Object.create(Teacher);
josue.teach("CSS"); 

//2
var Shape = {
	type : 'Default',
	getType : function() { console.log('I am a Shape type: ' + this.type) },
	getPerimeter : function(array) { var sum = 0; for(var i = 0; i < array.length; i++) {
										var sum = sum + array[i];
									}
										return sum; 
									} // pasar a reduce
};


function createTriangle(size_a, size_b, size_c){
	var triangle = Object.create(Shape, {

	a : {value : size_a },
	b : {value : size_b},
	c : {value : size_c },
	type : { value: 'Triangle' },
	size: {value : [size_a,size_b,size_c]}
	});
	return triangle;
}
var triangle1 = createTriangle(3,3,3);
triangle1.getType();
triangle1.getPerimeter(triangle1.size); 

// 3
var Employee = {
	name : '',
	dept : 'General',
	toString : function() { return 'Name: ' + this.name + ' Dept: ' + this.dept; }
};

var Manager = Object.create(Employee, {
	report : { value : []},
	addEmployee : { value : function(mp){
								console.log(mp);
								console.log(typeof mp);
								if(!mp || typeof mp != 'object'){
									return console.log('can not insert element employee');
							
								} else {
										return this.report.push(mp);
									}
							}	
				  },
	listEmployee : { value: function() {
						var arrayEmployee = [];
						console.log('List of Employees of Manager ' + this.name);
						arrayEmployee = this.report.reduce(function(valorAnterior, valorActual, indice, vector){
						  return  valorAnterior + ' ' + valorActual;
						});
            			console.log(arrayEmployee);
					} 
				}

});

var WorkerBee = Object.create(Employee, {
	proyects : { value : []},
	addProyect : { value : function(objProyect){

								if(!mp || typeof mp != 'string'){
									return console.log('can not insert element Proyect');
							
								} else {
										return this.proyects.push(mp);
									}
							}	
				  },
});


var SalesPerson = Object.create(WorkerBee, {
	quota : { value : 100 },
	dept : { value : 'Sales'}
});

var Engineer = Object.create(WorkerBee, {
	machine : { value : '' },
	dept : { value : 'engineering' }
});

var manager = Object.create(Manager);
manager.name = 'Esteban Brenes'; 
manager.dept = 'IT';
manager.toString(); 

var employee1 = Object.create(Employee);
employee1.name = 'Adrian Monge'; 
employee1.dept = 'IT';
employee1.toString(); 

var employee2 = Object.create(Employee);
employee2.name = 'Oscar Mario'; 
employee2.dept = 'Finances';
employee2.toString(); 

var employee3 = Object.create(Employee);
employee3.name = 'Adriana Moya'; 
employee3.dept = 'Creative';
employee3.toString(); 

var wokerbee1 = Object.create(WorkerBee);
wokerbee1.name = 'I am a workerbee'; 
wokerbee1.dept = 'Worker Dept';
wokerbee1.toString(); 

var salesPerson1 = Object.create(SalesPerson);
salesPerson1.name = 'Antonio Chichilla'; 
salesPerson1.toString(); 

var engineer1 = Object.create(Engineer);
salesPerson1.name = 'Michael Zumbado'; 
salesPerson1.toString(); 

manager.addEmployee(employee1);
manager.addEmployee(employee2);
manager.addEmployee(employee3);
manager.listEmployee();