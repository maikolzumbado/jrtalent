function Person(first, last, age) { 
	this.first = first; 
	this.last = last; 
	this.age = age; 
} 
Person.prototype = { 
	getFullName: function() { alert(this.first + ' ' + this.last); }, 
	greet: function(other) { alert("Hi " + other.first + ", I'm " + this.first + "."); 
	} 
};
var elodie = new Person('Elodie', 'Jaubert', 27); 
var christophe = new Person('Christophe', 'Porteneuve', 30);

function times(n, fx, arg) { 
	for (var index = 0; index < n; ++index) { 
		fx(arg);

	}
  
}
var showPerson = Person.prototype.greet;
var executePerson = showPerson.bind(christophe);
times(1,executePerson,elodie);


var executePerson2 = showPerson.bind(elodie);
times(1,executePerson2,christophe);
