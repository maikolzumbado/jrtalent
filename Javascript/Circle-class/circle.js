var ColoredCircle = function(dxN,dyN,dRadiusN,colorN){

  var color = colorN;
  var dx = dxN;
  var dy = dyN;
  var dRadius = dRadiusN;

  return {
    move : function(){
     console.log('moved!');
    },
    scale : function(scale1){ 
      dx = dx * scale1;
      dy = dy * scale1;
      console.log('Scale x: ' + dx + 'Scale Y: ' + dy);
    },
    toString: function(){
      console.log('I am a Circle x: ' + dx + 'y: ' + dy + ' radius: ' + dRadius + ' color: ' + color);
    },
    getColor: function(){
      return color;
    },
    setColor: function(newColor){
      color = newColor;
    },
    area: function(){
      var arearesult = 0;
      arearesult = 3.14 * dRadius * dRadius;
      console.log(arearesult);
    },
    perimeter: function(){
      var per = 2 * dRadius;
      console.log(per);
    }
   }
 };
var Circle1 = ColoredCircle(2,2,2,'red');
var Circle2 = ColoredCircle(3,3,3,'blue');
Circle1.move();
Circle1.getColor();
Circle1.toString();
Circle1.area();
Circle1.perimeter();
Circle1.setColor('YELLOW');
Circle1.toString();

