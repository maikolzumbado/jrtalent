//1
// select the <ul id="fruit-list"> element
var fruitList = document.querySelector("#fruit-list" );
   // get the element's id
console.log (fruitList.id);
// should print something like: // fruit-list

//2
// get the name of the element's tag
console.log(fruitList.tagName);
// should print something like: // UL

//3
// get the HTML contents of the element
console.log (fruitList.innerHTML);
// should print something like: 
// <li class="fruit">apple</li> 
// <li class="fruit">orange</li> 
// <li class="fruit">papaya</li> 
// <li class="fruit">durian</li>

//4
// get the text contents of the element
console.log(fruitList.innerText);
// should print something like: // apple
// orange
// papaya
// durian

//5
// get the HTML of the element (including itself + contents)
console.log(fruitList.outerHTML);
// should print something like: // <ul id="fruit-list">
//   <li class="fruit">apple</li>
//   <li class="fruit">orange</li>
//   <li class="fruit">papaya</li>
//   <li class="fruit">durian</li>
// </ul>

//6
// select the <h1 id="title"> tag
var  title  =   document.querySelector("#title");
var  currentTitle  =  title.innerText;
title.innerText  =   "Super "   +  currentTitle  +   "!" ;
// should change the text in the <h1 id="title"> tag to: // "Super Smoothie Time!"
var  title  =   document.querySelector("#title");
title.innerText  =   "Super "   +  title.innerText  +   "!" ;

//7

// select the "favorite fruit" tag
var favoriteFruit = document.querySelector(".fruit");
favoriteFruit.innerText  =   "strawberry" ;
// should change the text in the first <p> tag to:
// "Everyone likes smoothies. My favorite is strawberry."

//8
var fruits = document.querySelector("ul#fruit-list > li.fruit:first-child");
fruits.style.backgroundColor =   "orange";
fruits.style.color  =   "white";

//9
var fruits =  document.querySelectorAll("li.fruit");
var  papaya  =  fruits[2];
var  currentClasses  =  papaya.className;
papaya.className =  currentClasses  +   " highlight" ;
// should highlight the listing for "papaya" in yellow

//10
var favoriteFruit = document.querySelector("span.fruit");
favoriteFruit.className =   "";
// We can use classList but is not supported by newest browser

//11
var hideElement = function (element) {
 element.style.display = "none";
};

var showElement = function (element) {
 // this clears the display property so that the element inherits this style 
 element.style.display = "" ;
};

var toggleElement = function(element) {
  if(element.style.display === "none" ) {
  	showElement(element); 
  } else  {
  	hideElement(element);
  	}
};

var fruits = document.querySelectorAll("li.fruit" );
var papaya  = fruits[2];
setInterval ( function () {
  toggleElement (papaya); },  1000 );

//12
var parent = document.getElementById("div1");
var child = document.getElementById("p1");
parent.removeChild(child);

var fruits  =  document.querySelectorAll("li.fruit");
var durian  =  fruits[3];
durian.remove()

//13
var  fruits  =   document.querySelectorAll("li.fruit");
var  durian  =  fruits[3];
var  orange  =  fruits[1];
durian.remove();

var fruitList  = document.querySelector("ul#fruit-list");
// select parent element
fruitList.insertBefore(durian, orange); 
// insert durian in proper location (before orange) 
// should show the fruits in alphabetical orderer

var pomegranate = document.createElement( "li" ); 
// create new element 
pomegranate.innerText = "pomegranate";
  // set text of element 
pomegranate.className = "fruit"; 
 // set class of element
fruitList.appendChild(pomegranate);

//14
var subscribeForm  = document.querySelector("form.subscribe-form");  
console.log(subscribeForm.getAttribute("name"));

subscribeForm.setAttribute("action" ,"/subscribe");
 // set action attribute 
console.log(subscribeForm.getAttribute( "action" ));

//15
var subscribeEmail = subscribeForm.querySelector("input[name='email-address']"); 
subscribeEmail.focus();  
// set focus to email input field

//16
var  submitBtn  =  subscribeForm.querySelector("input[type='submit']");
submitBtn.setAttribute("value","JOIN ME");

//17
// Review
var body = document.querySelectorAll("body");
function getDescendants(element){
	console.log(element.childNodes);
	return getDescendants(element.childNodes);
}
getDescendants(body);
