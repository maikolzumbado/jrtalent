//1
	$("textarea").css("borderStyle", "solid 1px black");

	$("p,textarea").css("borderColor","red");
	$("p,textarea").css("borderStyle", "solid");

//2
$("<p>Hello World!</p>").appendTo("body");

 //3
$("p:last").addClass("w3r_font_color");

//4
$("p:last").addClass("w3r_font_color w3r_background");

//5
$("p:last").addClass("w3r_bg_red");

//6
$("<p>Hello User, josue was here!</p>").appendTo("p");

//7
$("*").size();
$("*").length;

//8
$("div").size();
$("div").length;

//9
$(document).ready(function myFunction(){
	alert($("div").size());
});

// 10
$("#button1").click(function(){
				$("#pid").animate({
					width: "70%",
					opacity: "0.4",
					marginLeft: "3em",
					fontSize: "3em",
					borderWidth: "10px"
				});
			});

// 11
			$("#left").click(function(){
				$(".block").animate({
					left: "+=150px"
				});
			});
			$("#right").click(function(){
				$(".block").animate({
					left: "-=150px"
				});
			});
//12
$("#run").click(function(){
				$(".block").animate({
					left: "+=150px"
				},
				{
                        duration: 500,
                        step: function( currentLeft ){
                        	$("block").css("left","=+150px");
                            console.log( "Left: ", currentLeft );
                        }
                    }
				);
			});
//13
