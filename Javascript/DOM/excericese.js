//1
function getElement(){
	return document.getElementById("title");

}
function getElement(){
	var x = document.querySelectorAll('#title');
	return x;
}


var title = getElement();
//2
function getLiFruits(){
	var x = document.querySelectorAll('li.fruit');
	return x;
}
var fruits = getLiFruits();

//3
var paragraph = document.querySelectorAll("p");
console.log(paragraph);

//Specific 
console.log(paragraph[1]);


// 4
var fruitList = document.querySelector("#fruit-list");//document.getElementById('fruit-list');
console.log(fruitList);

//5
//class
var  allFruit = document.querySelectorAll('.fruit');
console.log(allFruit);

//6
var fruitInTheList = document.querySelectorAll("ul#fruit-list > li.fruit");
console.log(fruitInTheList);

//7

/*
var form = document.forms[0];
var  subscribeEmail  =   document.querySelector('input[name="email-address"]');
var print = subscribeEmail.value;
console.log(print);
*/

var  subscribeEmail  =   document.querySelector('input[name="email-address"]');
console.log(subscribeEmail);

//8
var  fruitList  = document.querySelector("#fruit-list");
console.log (fruitList.children);
console.log (fruitList.parentElement.nodeName);
console.log (fruitList.parentElement);

//9
 //1
 var  paragraphs  = document.getElementsByTagName("p");
console.log(paragraphs);
//2
var  fruitList  = document.getElementById("fruit-list");
console.log(fruitList);
//3
var allFruit = document.getElementsByClassName("fruit");
console.log(allFruit);

//4
var fruitInTheList = document.getElementById( "fruit-list" ).getElementsByClassName("fruit");
console.log(fruitInTheList);

//5
var  subscribeEmail  =   document.getElementsByName('email-address');
console.log(subscribeEmail);
